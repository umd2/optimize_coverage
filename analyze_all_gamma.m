%  Analyze results from bulk_gamma_run.m
% 
% Author: Bryan Nousain, University of Maryland
% Date: December 16, 2021

clear all;clc;close all;
onlyReportPlots = false;

% optType = 'fmincon';
optType = 'greedy';
% optType = 'greedy-py';


if strcmpi(optType,'fmincon')
    results_path = './results';
%     results_path = './results/e223884_finalButWrongOutputShape';
elseif strcmpi(optType,'greedy')
    results_path = './results_greedy';
elseif strcmpi(optType,'greedy-py')
    %     results_path = './results_py';
    results_path = './results_py_59f1c15_final';
else
    error('The requested optimization type, %s, is not available.',optType);
end

% List all files in path
dirList = dir(fullfile(results_path,'*.mat'));
fileMask = not(cell2mat({dirList.isdir}));
fileList = {dirList.name};
fileList = fileList(fileMask);
numFiles = length(fileList);

if numFiles == 0
    error('No files to process in directory: %s',results_path);
end

coverPctUnsort = zeros(numFiles,1);
gammaUnsort = zeros(numFiles,1);
objUnsort = zeros(numFiles,1);
totDistUnsort = zeros(numFiles,1);
execTimeUnsort = zeros(numFiles,1);
iterationsUnsort = zeros(numFiles,1);
xIdxUnsort = zeros(numFiles,1);
x0Unsort = cell(numFiles,1);
solutionUnsort = cell(numFiles,1);

RECALC_coverPctUnsort = zeros(numFiles,1);
RECALC_objUnsort = zeros(numFiles,1);
RECALC_totDistUnsort = zeros(numFiles,1);

fileToFlag = [];%151;
normalexitflags = [1:5 -3]; % only for MATLAB optimization files (not Python)
for fIdx = 1:numFiles
    cf = load(fullfile(results_path,fileList{fIdx}));
    
    if strcmpi(optType,'fmincon') || strcmpi(optType,'greedy')
        coverPctUnsort(fIdx) = double(cf.coverPct);
        gammaUnsort(fIdx) = cf.gamma;
        totDistUnsort(fIdx) = cf.xd;
        xIdxUnsort(fIdx) = cf.xIdx;
        x0Unsort{fIdx} = cf.x0;
        objUnsort(fIdx) = cf.objectiveValue;
        solutionUnsort{fIdx} = cf.solution;
        
        execTimeUnsort(fIdx) = cf.execTimeSecs;
        iterationsUnsort(fIdx) = cf.output.iterations;
    elseif strcmpi(optType,'greedy-py')
        coverPctUnsort(fIdx) = double(cf.coverPct);
        gammaUnsort(fIdx) = double(cf.gamma);
        totDistUnsort(fIdx) = double(cf.xd);
        xIdxUnsort(fIdx) = double(cf.xIdx);
        x0Unsort{fIdx} = double(cf.x0(:));
        objUnsort(fIdx) = -double(cf.objectiveValue);
        solutionUnsort{fIdx} = double(cf.solution(:));
    end
    
    %     visualizeSoln(cf.solution,cf.r,cf.x0,cf.xmin,cf.xmax,cf.ymin,cf.ymax,cf.grid_incr,cf.ac,cf.bc);
    %     title(sprintf('ROI Covered: %.2f%%, Total Distance: %.2f, \\gamma = %.2f, fval = %.2f, # iterations = %d, exitflag = %d',...
    %         cf.coverPct,cf.xd, cf.gamma, cf.objectiveValue, cf.output.iterations, cf.exitflag));
    %     pause; % add pause, because MATLAB will crash when too many plots are created in a short amount of time
    
    if ~isempty(fileToFlag)
        if fIdx == fileToFlag
            flaggedFile=cf;
            flaggedFile.fn = fileList{fIdx};
        end
    end
    %     if ~ismember(cf.exitflag,normalexitflags)
    %         keyboard;
    %     end
    
    if strcmpi(optType,'greedy-py')
        %% Recalculate objective function values
        optimInput.r = double(cf.r);
        optimInput.xmin = double(cf.xmin);
        optimInput.xmax = double(cf.xmax);
        optimInput.ymin = double(cf.xmin);
        optimInput.ymax = double(cf.xmax);
        optimInput.grid_incr =  double(cf.grid_incr);%5e-2; % same grid used in MATLAB implementation %
        optimInput.roi_a = double(cf.ac);
        optimInput.roi_b = double(cf.bc);
        optimInput.xi = double(cf.x0(:));
        optimInput.gamma = double(cf.gamma);
        x = double(cf.solution(:));
        
        objectiveValue = objectiveFcn(x,optimInput);
        
        x = [x(1:end/2), x(end/2+1:end)];
        xi = [optimInput.xi(1:end/2), optimInput.xi(end/2+1:end)];
        
        xd = sum(sqrt(sum((x - xi).^2,2))); % sum of distance travelled
        xd2 = sum(sum((x - xi).^2,2)); % sum of (distance travelled)^2
        coverPct = -1*(objectiveValue - optimInput.gamma*xd2);
        
        RECALC_coverPctUnsort(fIdx) = coverPct;
        RECALC_objUnsort(fIdx) = objectiveValue;
        RECALC_totDistUnsort(fIdx) = xd;
    elseif strcmpi(optType,'greedy')
        %% Recalculate objective function values because the grid for the greedy function was very coarse
        optimInput.r = cf.r;
        optimInput.xmin = cf.xmin;
        optimInput.xmax = cf.xmax;
        optimInput.ymin = cf.xmin;
        optimInput.ymax = cf.xmax;
        optimInput.grid_incr =  5e-3;%cf.grid_incr;%5e-3; % same grid used in MATLAB implementation %
        optimInput.roi_a = cf.ac;
        optimInput.roi_b = cf.bc;
        optimInput.xi = cf.x0;
        optimInput.gamma = cf.gamma;
        x = cf.solution;
        
        objectiveValue = objectiveFcn(x,optimInput);
        
        x = [x(1:end/2), x(end/2+1:end)];
        xi = [optimInput.xi(1:end/2), optimInput.xi(end/2+1:end)];
        
        xd = sum(sqrt(sum((x - xi).^2,2))); % sum of distance travelled
        xd2 = sum(sum((x - xi).^2,2)); % sum of (distance travelled)^2
        coverPct = -1*(objectiveValue - optimInput.gamma*xd2);
        
        RECALC_coverPctUnsort(fIdx) = coverPct;
        RECALC_objUnsort(fIdx) = objectiveValue;
        RECALC_totDistUnsort(fIdx) = xd;
    end
end

%% Plot Flagged File
if ~isempty(fileToFlag)
    if fileToFlag > numFiles
        warning('File index requested to be flagged (%d) is greater than the number of files (%d)',fileToFlag,numFiles);
    else        
        if strcmpi(optType,'greedy-py')
            flaggedFile.objectiveValue = double(flaggedFile.objectiveValue);
            flaggedFile.xIdx = double(flaggedFile.xIdx);
            flaggedFile.gamma = double(flaggedFile.gamma);
            flaggedFile.coverPct = double(flaggedFile.coverPct);
            flaggedFile.xd = double(flaggedFile.xd);
            flaggedFile.x0 = double(flaggedFile.x0(:));
            
            flaggedFile.solution = double(flaggedFile.solution(:));
            flaggedFile.r = double(flaggedFile.r);
            
            flaggedFile.ac = double(flaggedFile.ac);
            flaggedFile.bc = double(flaggedFile.bc);
            flaggedFile.grid_incr = double(flaggedFile.grid_incr);
            
            flaggedFile.xmin = double(flaggedFile.xmin);
            flaggedFile.xmax = double(flaggedFile.xmax);
            
            flaggedFile.ymin = flaggedFile.xmin;
            flaggedFile.ymax = flaggedFile.xmax;
            flaggedFile.output.iterations = NaN;
            flaggedFile.exitflag = NaN;
        elseif strcmpi(optType,'fmincon')
            flaggedFile.x0 = flaggedFile.x0(:); % temporary, runs after 12/15/21, 1526 will not need this, only need for results generated by git hash e223884
            flaggedFile.solution = flaggedFile.solution(:); % temporary, runs after 12/15/21, 1526 will not need this, only need for results generated by git hash e223884
        end
        
        visualizeSoln(flaggedFile.solution,flaggedFile.r,flaggedFile.x0,flaggedFile.xmin,flaggedFile.xmax,flaggedFile.ymin,flaggedFile.ymax,flaggedFile.grid_incr,flaggedFile.ac,flaggedFile.bc);
        
        title(sprintf('ROI Covered: %.2f%%, Total Distance: %.2f, \\gamma = %.2f, fval = %.2f, # iterations = %d, exitflag = %d',...
            flaggedFile.coverPct,flaggedFile.xd, flaggedFile.gamma, flaggedFile.objectiveValue, flaggedFile.output.iterations, flaggedFile.exitflag));
        
        % Short title
        title(sprintf('Region of Interest Covered: %.2f%%, Total Distance: %.2f, \\gamma = %.2f',...
            flaggedFile.coverPct,flaggedFile.xd, flaggedFile.gamma));        
    end
end
%% Sort all parameters by ascending gamma
[gamma, gammaSrtIdx] = sort(gammaUnsort,'ascend');


        
coverPct = coverPctUnsort(gammaSrtIdx);
totDist = totDistUnsort(gammaSrtIdx);
execTime = execTimeUnsort(gammaSrtIdx);
iterations = iterationsUnsort(gammaSrtIdx);
xIdx = xIdxUnsort(gammaSrtIdx);
x0 = x0Unsort(gammaSrtIdx);
obj = objUnsort(gammaSrtIdx);

% The greedy algorithms used a larger grid size, so recalculate the metrics
% using the same grid as fmincon
if strcmpi(optType,'greedy') || strcmpi(optType,'greedy-py')    
    coverPct = RECALC_coverPctUnsort(gammaSrtIdx);
    obj = RECALC_objUnsort(gammaSrtIdx);
end
%% Calulate statistics to plot
[medCvrPerc, gamma_uniq]= groupsummary(coverPct,gamma,'median');
[stdCvrPerc, gamma_uniq] = groupsummary(coverPct,gamma,'std');

[medTotTrav, gamma_uniq] = groupsummary(totDist,gamma,'median');
[stdTotTrav, gamma_uniq] = groupsummary(totDist,gamma,'std');

[medObj, gamma_uniq] = groupsummary(obj,gamma,'median');
[stdObj, gamma_uniq] = groupsummary(obj,gamma,'std');

baselineTotDist = totDist(gamma == 0); % baseline distance for each starting position
baselineToTDistxIdx = xIdx(gamma == 0);

percLessDist = totDist;
for xIter = 1:length(baselineToTDistxIdx)
    percLessDist(xIdx == baselineToTDistxIdx(xIter)) = totDist(xIdx == baselineToTDistxIdx(xIter))/baselineTotDist(xIter)*100;
end

[medPercLessTrav, gamma_uniq]= groupsummary(percLessDist,gamma,'median');
[stdPercLessTrav, gamma_uniq] = groupsummary(percLessDist,gamma,'std');

%% Make plots
% Plotting parameters
lw = 2;
ms = 15;
plotPath = './img';

if ~exist(plotPath,'dir')
    mkdir(plotPath);
end
    
% Create optimization technique descriptor to append to plot titles
if strcmpi(optType,'fmincon')
    optString = 'BFGS';
elseif strcmpi(optType,'greedy') || strcmpi(optType,'greedy-py')
    optString = 'Greedy';
else
    optString = 'Unknown';
end
    
% Make plots
fprintf('Run time statistics over %d runs\n',length(execTime));

if strcmpi(optType,'fmincon') || strcmpi(optType,'greedy')
    fprintf('\t Average time: %.2f seconds (%.2f minutes)\n',mean(execTime),mean(execTime)/60);
    fprintf('\t Minimum time: %.2f seconds (%.2f minutes)\n',min(execTime),min(execTime)/60);
    fprintf('\t Maximum time: %.2f seconds (%.2f minutes)\n\n',max(execTime),max(execTime)/60);
    fprintf('\t Average iterations: %.2f\n',mean(iterations));
    fprintf('\t Minimum iterations: %d\n',min(iterations));
    fprintf('\t Maximum iterations: %d\n',max(iterations));
    
    if ~onlyReportPlots
        figure;
        histogram(execTime/60)
        xlabel('duration (minutes)')
        ylabel('counts');
        title(sprintf('Histogram of Optimization Time - %s',optString));
        grid on;
        set(gcf,'Color','w');       
        
        figure;
        histogram(iterations)
        xlabel('# iterations')
        ylabel('counts');
        title(sprintf('Histogram of Iterations Until Convergence - %s',optString));
        grid on;
        set(gcf,'Color','w');
    end
    if onlyReportPlots
        % used when plotting histrograms from multiple optimization algorithms on the same figure
        [itrCounts, itrEdges] = histcounts(iterations); 
        itrBinCenters = filter([.5 .5],1,itrEdges); % take moving average of edges to get bin centers
        itrBinCenters = itrBinCenters(2:end);
        
%         plotFN = fullfile(plotPath,sprintf('histogram_iterations_%s',optType));
%         savePlots(gcf,plotFN);
    end
end

if strcmpi(optType,'greedy-py') || strcmpi(optType,'greedy')
    %% Compare values of coverPct, objective, and total distance travelled from
    % files and recalculate
    coverPctDIFF = coverPctUnsort - RECALC_coverPctUnsort;
    totDistDIFF = totDistUnsort - RECALC_totDistUnsort;
    objDIFF = objUnsort - RECALC_objUnsort;
    
    fnParse = split(fileList{1},'_');
    gitHashFN = fnParse{3};
    
    if ~onlyReportPlots
        figure;
        plot(coverPctDIFF,'LineWidth',lw);
        hold all;
        plot(totDistDIFF,'LineWidth',lw);
        plot(objDIFF,'LineWidth',lw);
        legend({'Cover %','Total distance','Objective function'},'Location','Best');
        grid on;
        xlabel('file index');ylabel('error');
        title(sprintf('Compare Recalculated Metrics and Data File Metrics - Git Hash: %s',gitHashFN));
        set(gcf,'Color','w');
    end
    
end
%% Triple check calculations
% ffx=[flaggedFile.x0(1:end/2) flaggedFile.x0(end/2+1:end)];
%
% ffs=[flaggedFile.solution(1:end/2) flaggedFile.solution(end/2+1:end)];
%
% rc_xd2 = sum(sum((ffx-ffs).^2,2));
% rc_xd1 = sum(sqrt(sum((ffx-ffs).^2,2)));
%
% RR_obj = -RECALC_coverPctUnsort(fileToFlag) + flaggedFile.gamma*rc_xd2;
%
% if  RECALC_objUnsort(fileToFlag) == RR_obj
%     disp('MATCH. Re-calculation of objective function using cover percent and starting and final positions from Python file is the same as recalculating with objectiveFnc.m');
% else
%     disp('NO MATCH. Re-calculation of objective function using cover percent and starting and final positions from Python file is NOT the same as recalculating with objectiveFnc.m');
%
% end

% objUnsort(fileToFlag)
%% Plot objective value, area covered, distance saved vs gamma
gammaDecimateFactor = 5;

figure;
if ~onlyReportPlots
    errorbar(gamma_uniq,medObj,stdObj/2,stdObj/2,'LineWidth',lw);
    title('objective value vs \gamma, Error bars at \pm 0.5 standard dev.');
else % plot subset of gammas, because plot is too cluttered
    gammaIdxToPlot = 1:gammaDecimateFactor:length(gamma_uniq);
    errorbar(gamma_uniq(gammaIdxToPlot),medObj(gammaIdxToPlot),stdObj(gammaIdxToPlot)/2,stdObj(gammaIdxToPlot)/2,'LineWidth',lw);
    title(sprintf('objective value vs \\gamma - %s - \\pm 0.5 standard dev. error bars',optString));
end
grid on;
set(gcf,'Color','w');
xlabel('\gamma')
ylabel('objective value');
% axis tight
% cyl = get(gca,'YLim');
% ylim([cyl(1), 101]); % limit y axis to 101

if onlyReportPlots
    plotFN = fullfile(plotPath,sprintf('fval_vs_gamma_%s',optType));
%     savePlots(gcf,plotFN);
end


figure;
if ~onlyReportPlots
    errorbar(gamma_uniq,medCvrPerc,stdCvrPerc/2,stdCvrPerc/2,'LineWidth',lw);
        title('% area covered vs \gamma, Error bars at \pm 0.5 standard dev.');
else % plot subset of gammas, because plot is too cluttered
    gammaIdxToPlot = 1:gammaDecimateFactor:length(gamma_uniq);
    errorbar(gamma_uniq(gammaIdxToPlot),medCvrPerc(gammaIdxToPlot),stdCvrPerc(gammaIdxToPlot)/2,stdCvrPerc(gammaIdxToPlot)/2,'LineWidth',lw);
    title(sprintf('%% area covered vs \\gamma - %s - \\pm 0.5 standard dev. error bars',optString));
end
grid on;
set(gcf,'Color','w');
xlabel('\gamma')
ylabel('% of area covered');
% axis tight
cyl = get(gca,'YLim');
ylim([cyl(1), 101]); % limit y axis to 101

if onlyReportPlots
    plotFN = fullfile(plotPath,sprintf('area_vs_gamma_%s',optType));
%     savePlots(gcf,plotFN);
end



figure;
if ~onlyReportPlots    
    errorbar(gamma_uniq,medPercLessTrav,stdPercLessTrav/2,stdPercLessTrav/2,'LineWidth',lw);
    title('% less distance travelled vs \gamma, Error bars at \pm 0.5 standard dev.');
else
    gammaIdxToPlot = 1:gammaDecimateFactor:length(gamma_uniq);
    errorbar(gamma_uniq(gammaIdxToPlot),medPercLessTrav(gammaIdxToPlot),stdPercLessTrav(gammaIdxToPlot)/2,stdPercLessTrav(gammaIdxToPlot)/2,'LineWidth',lw);
    title(sprintf('%% less distance travelled vs \\gamma - %s - \\pm 0.5 standard dev. error bars',optString));
end
grid on;
set(gcf,'Color','w');
xlabel('\gamma')
ylabel('% less distance travelled');

if onlyReportPlots
    plotFN = fullfile(plotPath,sprintf('percLessTotDist_vs_gamma_%s',optType));
%     savePlots(gcf,plotFN);
end

if ~onlyReportPlots
    figure;
    errorbar(gamma_uniq,medTotTrav,stdTotTrav/2,stdTotTrav/2,'LineWidth',lw);
    grid on;
    set(gcf,'Color','w');
    xlabel('\gamma')
    ylabel('total distance travelled');
    title('Total distance travelled vs \gamma, Error bars at \pm 0.5 standard dev.');
end
%% Check for missing gammas
numGammasPerNode  = 10; % from generate_shell_scripts.m
numTrials = 15; % from generate_shell_scripts.m
minGamma = 0;
maxGamma = 2;
gammaPrecision = 4; % number of digits

if ~onlyReportPlots
    figure;
    histogram(gamma,numTrials*numGammasPerNode);
    title('Num runs per \gamma');
    ylabel('\gamma');
    xlabel('counts');
    set(gcf,'Color','w');
end
[gammaCounts, gammaEdges] = histcounts(gamma,floor(linspace(minGamma,maxGamma,numTrials*numGammasPerNode)*10^gammaPrecision)/10^gammaPrecision);

missingGammaIdx = find(gammaCounts ~= numTrials);

allxIdx = 1:numTrials; % set of complete xIdx
missingxIdx = cell(length(missingGammaIdx),1);
for mgIdx = 1:length(missingGammaIdx)
    % Find all gammas
    gIdx = find(gammaUnsort == gammaEdges(missingGammaIdx(mgIdx)));
    
    % Check to which xIdx values are included for a particular gamma
    allxIdxMask = ~ismember(allxIdx,xIdxUnsort(gIdx));
    missingxIdx{mgIdx} = allxIdx(allxIdxMask);
    fprintf('Missing gamma: %.4f\n',gammaEdges(missingGammaIdx(mgIdx)));
    fprintf('\tMissing xIdx: %d\n',missingxIdx{mgIdx});
end

numMissingFiles = sum(cellfun('length',missingxIdx));
fprintf('There are %d missing files!\n',numMissingFiles);
%% Check for missing trials (xIdx)
% figure;
% histogram(xIdx,numTrials);
% title('Num runs per xIdx');
% ylabel('xIdx');
% xlabel('counts');
% set(gcf,'Color','w');
% 
% [xIdxCounts, xIdxEdges] = histcounts(xIdx,1:numTrials);
% 
% missingxIdxIdx = find(xIdxCounts ~= numTrials*numGammasPerNode);
% fprintf('Missing xIdx: %.4f\n',xIdxEdges(missingxIdxIdx));

%% Plot area covered vs extra distance travelled
% if ~onlyReportPlots
%     figure;
% end
% 
% lgnd = cell(length(xIdx_uniq),1);
% percMoreDist = totDist;
% for xIter = 1:length(xIdx_uniq)
%     percMoreDist(xIdx == xIdx_uniq(xIter)) = totDist(xIdx == xIdx_uniq(xIter))/minTotTrav(xIter)*100; % find the extra percentage of distance travelled
%     
%     if ~onlyReportPlots
%         plot(totDist(xIdx == xIdx_uniq(xIter))/minTotTrav(xIter)*100,coverPct(xIdx == xIdx_uniq(xIter)),'.','MarkerSize',15,'LineWidth',lw);
%         %     loglog(totDist(xIdx == xIdx_uniq(xIter))/minTotTrav(xIter)*100,coverPct(xIdx == xIdx_uniq(xIter)),'.','MarkerSize',15,'LineWidth',lw);
%         hold all;
%     end
%     lgnd{xIter} = num2str(xIdx_uniq(xIter));
% end
% if ~onlyReportPlots    
%     grid on;
%     set(gcf,'Color','w');
%     xlabel('% more distance travelled')
%     ylabel('% of area covered');
%     title('Area Covered vs Distance Travelled');
%     hl = legend(lgnd,'Location','Best');
%     title(hl,'Initial Location Set')
% end

%% Plot area covered vs extra distance travelled with horizontal error bars, will quantize area covered
% close all;
binSize = 100/6; % in percent
coverPctEdges = -binSize/2:binSize:(100+binSize);
% For binSize = 1
%   Bin 1 is 0, 0 to 0.4999
%   Bin 2 is 1, 0.5 to 1.4999
%   ...
%   Bin 100 is 99, 98.5 to 99.4999
%   Bin 101 is 100, 99.5 to 99.9999
%   Bin 102 is also 100

coverPctBinCenters = [0:binSize:100 100];

[coverPctCounts, ~, coverPctBins] = histcounts(coverPct, coverPctEdges);
coverPctQ = coverPctBinCenters(coverPctBins)';

[stdPercTotTravQ, coverPctQUniq] = groupsummary(percLessDist,coverPctQ,'std');
medPercTotTravQ = groupsummary(percLessDist,coverPctQ,'median');

[medPercTotTravQSrt, medPercTotTravQIdx] = sort(medPercTotTravQ,'ascend');

if ~onlyReportPlots    
    figure;
    errorbar(medPercTotTravQSrt,coverPctQUniq(medPercTotTravQIdx),stdPercTotTravQ/2,stdPercTotTravQ/2,'horizontal','LineWidth',lw);
    grid on;
    set(gcf,'Color','w');
    xlabel('% more distance travelled')
    ylabel('% of area covered');
    title('% area covered vs % more distance travelled, Error bars at \pm 0.5 standard dev.');
end
%% Plot area covered vs extra distance travelled with vertical error bars, will quantize area covered
% close all;
binSize = 100/6; % in percent, empirically determined
coverPctEdges = -binSize/2:binSize:(100+binSize);
% For binSize = 1
%   Bin 1 is 0, 0 to 0.4999
%   Bin 2 is 1, 0.5 to 1.4999
%   ...
%   Bin 100 is 99, 98.5 to 99.4999
%   Bin 101 is 100, 99.5 to 99.9999
%   Bin 102 is also 100

coverPctBinCenters = [0:binSize:100 100];

[coverPctCounts, ~, coverPctBins] = histcounts(coverPct, coverPctEdges);
coverPctQ = coverPctBinCenters(coverPctBins)';

[stdPercTotTravQ, coverPctQUniq] = groupsummary(percLessDist,coverPctQ,'std');
medPercTotTravQ = groupsummary(percLessDist,coverPctQ,'median');

[medPercTotTravQSrt, medPercTotTravQIdx] = sort(medPercTotTravQ,'ascend');
figure;
if ~onlyReportPlots
    errorbar(medPercTotTravQSrt,coverPctQUniq(medPercTotTravQIdx),stdPercTotTravQ/2,stdPercTotTravQ/2,'LineWidth',lw);
    title('% area covered vs % more distance travelled, Error bars at \pm 0.5 standard dev.');
else
    plot(medPercTotTravQSrt,coverPctQUniq(medPercTotTravQIdx),'Marker','.','MarkerSize',ms,'LineWidth',lw);
    title(sprintf('%% area covered vs %% less distance travelled - %s',optString));
    
    if false % included here for manual analysis
        hold all;
        for xIter = 1:length(xIdx_uniq)
            percMoreDist(xIdx == xIdx_uniq(xIter)) = totDist(xIdx == xIdx_uniq(xIter))/minTotTrav(xIter)*100; % find the extra percentage of distance travelled
            plot(totDist(xIdx == xIdx_uniq(xIter))/minTotTrav(xIter)*100,coverPct(xIdx == xIdx_uniq(xIter)),'.','MarkerSize',15,'LineWidth',lw);
            %     loglog(totDist(xIdx == xIdx_uniq(xIter))/minTotTrav(xIter)*100,coverPct(xIdx == xIdx_uniq(xIter)),'.','MarkerSize',15,'LineWidth',lw);
            hold all;
        end
        lgnd{xIter} = num2str(xIdx_uniq(xIter));
    end
end
grid on;
set(gcf,'Color','w');
xlabel('% more distance travelled')
ylabel('% of area covered');
% axis tight

if onlyReportPlots
    plotFN = fullfile(plotPath,sprintf('area_vs_dist_%s',optType));
%     savePlots(gcf,plotFN);
    
    varFN = sprintf('plot_vars_%s',optType);
    save(varFN)
end