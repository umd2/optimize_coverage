function [coverPct, xd, objectiveValue, solution, exitflag, output] = ouc(n,x0,r,ac,bc,gamma,xmin,xmax,ymin,ymax,grid_incr,enablePlots,use_parallel_computing_toolbox,numberWorkers)
% function [coverPct, xd, objectiveValue, solution, exitflag, output] = ouc(x0,number_uavs,r,ac,bc,gamma,xmin,xmax,ymin,ymax,grid_incr,enablePlots,use_parallel_computing_toolbox,numberWorkers)
% 
% Inputs:
%   n (double): number of unmanned air vehicles (UAVs) to provide coverage
%   x0 (double vector, dim: 2*n x 1): starting positions of each UAV, indicies 1 to n/2 are the x coordinates, n/2+1 to n are the y coordinates
%   r (double): sensing radius of individual UAV
%   ac (double matrix, dim: m x 2): Specifies the region of interest. For m constraints, the ith constrain ac(1,i)*x + ac(2,i)*y <= bc(i)
%   bc (double matrix, dim: m x 2): Specifies the region of interest. For m constraints, the ith constrain ac(1,i)*x + ac(2,i)*y <= bc(i)
%   gamma (double): Weight placed on total distance travelled
%   xmin (double): minimum x coordinate to consider during optimization
%   xmax (double): maximum x coordinate to consider during optimization
%   ymin (double): minimum y coordinate to consider during optimization
%   ymax (double): maximum y coordinate to consider during optimization
%   grid_incr (double): increment of grid  between xmin and xmax and ymin and ymax
%   enablePlots (boolean): if true will plot intermediate optimization progress and final results
%   use_parallel_computing_toolbox (boolean): if true will use parallel computing toolbox
%   numberWorkers (double) [OPTIONAL]: number of workers for parallel computing toolbox to use. The default number of workers is 4.
%
% Outputs:
%   coverPct (double): percentage of region of interest covered
%   xd (double): total distance travelled
%   objectiveValue (double): objective value at end of optimization
%   solution (double vector, dim: 2*x x 1): final positions of each UAV
%   exitflag: exitflag from FMINCON. See FMINCON documentation for more details
%   output: output struct from FMINCON. See FMINCON documentation for more details
% 
% Author: Bryan Nousain, University of Maryland
% Date: December 16, 2021
%%
if nargin < 14
    if use_parallel_computing_toolbox
        numberWorkers = 4;
    else
        numberWorkers = 0;
    end
end
% Shutting down the pool in case one is accidentially left open
p = gcp('nocreate');

if ~isempty(p)
    delete(gcp)
end
%% %%%%%%%% Define problem parameters %%%%%%%%
% number_uavs = 10;
% use_parallel_computing_toolbox = true;

% Define grid parameters
optimInput.xmin = xmin;
optimInput.xmax = xmax;
optimInput.ymin = ymin;
optimInput.ymax = ymax;
optimInput.grid_incr = grid_incr;
optimInput.r = r;
optimInput.gamma = gamma;

% % Define grid parameters
% optimInput.xmin = -7;
% optimInput.xmax = 5;
% optimInput.ymin = -7;
% optimInput.ymax = 5;
% optimInput.grid_incr = .5e-2;
%
% optimInput.r = 1;
% optimInput.gamma = 1e-1;

% % Define solution vector:
% % Points 1 to n/2 are the x coordinates, n/2+1 to n are the y coordinates
% % n is the number of uavs
% rng('default');
% x0 = (optimInput.xmax - optimInput.xmin)*rand(2*number_uavs,1) + optimInput.xmin;

% Load solutions from previous iterations
% load('x0.mat');
% load('x0s.mat');
% load('x0s2.mat');
% load('x0s3_18.mat');
% load('x0s3_18p07.mat');
% load('x0s3_18p11.mat');
% load('x0s3_18p24.mat');

optimInput.xi = x0; % save starting positions

%% %%%%%%%% Define region of interest, A*x <= b %%%%%%%%
% Specify region of interest according to
% ac(1,.)*x + ac(2,.)*y <= bc

%% %%%%%%%% Square region of interest %%%%%%%%
% % left/right boundary: 0 <= x <= 10
% % A*x >= b => - A*x <= -b
% leftBnd = 0;
% rightBnd = 5;
%
% % top/bottom boundary: 0 <= y <= 10
% bottomBnd = 0;
% topBnd = 5;
%
% ac = [...
%     -1 0;...
%     0 -1;...
%     1 0;...
%     0 1;...
%     ]';
% bc = [...
%     -leftBnd;...
%     -bottomBnd;...
%     rightBnd;...
%     topBnd];

%% %%%%%%%% Polytope region of interest %%%%%%%%
% ac = [...
%     2 10;...
%     3 4;...
%     -1 -1;...
%     1 -1;...
%     ]';
% bc = [...
%     5;...
%     5;...
%     5;...
%     5;...
%     ];

optimInput.roi_a = ac;
optimInput.roi_b = bc;

%% %%%%%%%% Create vectorized version of constraint functions A*x, b %%%%%%%%
if length(bc) ~= size(ac,2)
    error('Constraint mismatch: The number of rows in b (%d) must match the number of columns of a (%d).',length(bc), size(ac,2));
end

% Define matricies that will be used to apply constraints to coordinates for every UAV
xM = [diag(ones(n,1)) diag(zeros(n,1))];
yM = [diag(zeros(n,1)) diag(ones(n,1))];
bV = ones(n,1);
 
% initialize A and b
A = []; % not preallocating A, since there aren't expected to be many constraints
b = []; % not preallocating b, since there aren't expected to be many constraints
for cnIdx = 1:length(bc)
    A = [A;...
        ac(1,cnIdx)*xM + ac(2,cnIdx)*yM...
        ];
    b = [b;...
        bc(cnIdx)*bV...
        ];
end
%% Check if matrix 'A' and vector 'b' were correctly created (for debugging purposes only)

% % Create mesh to evaluate conditions on all points
% [grid_x, grid_y] = meshgrid(optimInput.xmin:optimInput.grid_incr:optimInput.xmax,...
%     optimInput.ymin:optimInput.grid_incr:optimInput.ymax);
%
% % Initialize in_intersection
% in_intersection = true(size(grid_x));
%
% for constrnt_idx = 1:size(b,1)
%     A_x_coeff = A(constrnt_idx,1:number_uavs);
%     if sum(A_x_coeff ~= 0) > 1
%         error('More than one coefficient for constraint on x')
%     end
%     x_coeff = A_x_coeff(A_x_coeff~=0);
%     if isempty(x_coeff)
%         x_coeff = 0;
%     end
%
%     A_y_coeff = A(constrnt_idx,number_uavs+1:end);
%     if sum(A_y_coeff ~= 0) > 1
%         error('More than one coefficient for constraint on y')
%     end
%     y_coeff = A_y_coeff(A_y_coeff~=0);
%     if isempty(y_coeff)
%         y_coeff = 0;
%     end
%
%     pointInCurrConstraint = (x_coeff*grid_x + y_coeff*grid_y) <= b(constrnt_idx);
%
%     in_intersection = in_intersection & pointInCurrConstraint;
%
% %     % Shade cumulative intersection of constraints
% %     imagesc(grid_x(1,:),grid_y(:,1),in_intersection)
% %     set(gca,'YDir','normal');
% %     title(sprintf('Verify correctness of ineqaulity parameters (Ax <= b), Constraint: %d',constrnt_idx))
% %
% %     const_color = 'g';
% %     const_lw = 3;
% %     hold all;
% %     for ai = 1:size(ac,2)
% %         lgnd{ai} = sprintf('%d *x + %d*y + %d\n',ac(1,ai),ac(2,ai),-bc(ai));
% %
% %         if ac(2,ai) == 0 % this means the boundary is a verical line
% %             lh = line([bc(ai)/ac(1,ai) bc(ai)/ac(1,ai)],[min(grid_y(1:end,1)) max(grid_y(1:end,1))],'Color',const_color,'LineWidth',const_lw);
% %         else
% %             plot(grid_x(1,1:end),-ac(1,ai)/ac(2,ai)*grid_x(1,1:end) + bc(ai)/ac(2,ai),const_color,'LineWidth',const_lw);hold all;
% %         end
% %     end
% %
% %     pause(.5);
% end
%
% % Shade intersection of linear constraints
% figure;
% imagesc(grid_x(1,:),grid_y(:,1),in_intersection)
% set(gca,'YDir','normal');
% title('Verify correctness of ineqaulity parameters (Ax <= b)')
%
% % Plot constraints
% const_color = 'g';
% const_lw = 3;
% hold all;
% for ai = 1:size(ac,2)
%     lgnd{ai} = sprintf('%d *x + %d*y + %d\n',ac(1,ai),ac(2,ai),-bc(ai));
%
%     if ac(2,ai) == 0 % this means the boundary is a verical line
%         lh = line([bc(ai)/ac(1,ai) bc(ai)/ac(1,ai)],[min(grid_y(1:end,1)) max(grid_y(1:end,1))],'Color',const_color,'LineWidth',const_lw);
%     else
%         plot(grid_x(1,1:end),-ac(1,ai)/ac(2,ai)*grid_x(1,1:end) + bc(ai)/ac(2,ai),const_color,'LineWidth',const_lw);hold all;
%     end
% end
%% Configure and run optimization

% Pass fixed parameters to objfun
objfun = @(x)objectiveFcn(x,optimInput);
intoutputfun = @(x,optimValues,state) intermediateOutputFcn(x,optimValues,state,optimInput,ac,bc);

% Define bounds of search space for x
lb = optimInput.xmin .* ones(2*n, 1);
ub = optimInput.xmax .* ones(2*n, 1);

% Set nondefault solver options
if enablePlots
    if use_parallel_computing_toolbox
        options = optimoptions('fmincon',...
            'OutputFcn',intoutputfun,...
            'FiniteDifferenceType', 'central', ...
            'Display', 'iter-detailed', ...
            'UseParallel','always',...
            'MaxFunEvals',20e3,...
            'Diagnostics', 'on');
        
        % 1 worker: 218.660801 seconds, 30 iterations, Fval = -1.688351e+01
        % 2 workers: 401.961202 seconds, 30 iterations, Fval = -1.688351e+01
        % 4 workers: 249.987749 seconds, 30 iterations, Fval = -1.688351e+01
        % 8 workers: 172.813349 seconds, 30 iterations, Fval = -1.688351e+01
        % 12 workers: 147.258750 seconds, 30 iterations, Fval = -1.688351e+01
        % 16 workers: 142.536959 seconds, 30 iterations, Fval = -1.688351e+01
        p = parpool(numberWorkers); % open pool
    else
        options = optimoptions('fmincon',...
            'OutputFcn',intoutputfun,...
            'FiniteDifferenceType', 'central', ...
            'Display', 'iter-detailed', ...
            'MaxFunEvals',20e3,...
            'Diagnostics', 'on');
    end
else
    if use_parallel_computing_toolbox
        options = optimoptions('fmincon',...
            'FiniteDifferenceType', 'central', ...
            'Display', 'iter-detailed', ...
            'UseParallel','always',...
            'MaxFunEvals',20e3,...
            'Diagnostics', 'on');
        
        p = parpool(numberWorkers); % open pool
    else
        options = optimoptions('fmincon',...
            'FiniteDifferenceType', 'central', ...
            'Display', 'iter-detailed', ...
            'MaxFunEvals',20e3,...
            'Diagnostics', 'on');
    end
end

% Solve
% x =                       fmincon(fun,   x0,A,b,Aeq,beq,lb,ub,nonlcon,options)
[solution,objectiveValue,exitflag,output] = fmincon(objfun,x0,A,b,[],[],lb,ub,[],options);

solution_2C = [solution(1:end/2), solution(end/2+1:end)];
x0_2C = [x0(1:end/2), x0(end/2+1:end)];
        
xd = sum(sqrt(sum((solution_2C - x0_2C).^2,2))); % sum of distance travelled
xd2 = sum(sum((solution_2C - x0_2C).^2,2)); % sum of (distance travelled)^2
coverPct = -1*(objectiveValue - optimInput.gamma*xd2);

%% %%%%%%%% Clean up workspace %%%%%%%%
% Clear variables
clearvars objfun options
if use_parallel_computing_toolbox
    delete(p); % close parallel pool
end
end
%% %%%%%%%% Helper functions %%%%%%%%
function stop = intermediateOutputFcn(x,optimValues,state,optimInput,ac,bc)
stop = false;

switch state
    case 'init'
        figure;
        
    case 'iter'
        
    case 'done'
        hold off
        
    otherwise
end

visualizeSoln(x,optimInput.r,optimInput.xi,optimInput.xmin,optimInput.xmax,optimInput.ymin,optimInput.ymax,optimInput.grid_incr,ac,bc,false)
visualizeGradient(x, optimValues);
pause(.01);
title(sprintf('Iteration %d, Objective: %.3f',optimValues.iteration,optimValues.fval));

end
function visualizeGradient(x, optimValues)
grad_x = [optimValues.gradient(1:end/2), optimValues.gradient(end/2+1:end)];
x = [x(1:end/2), x(end/2+1:end)];

quiver(x(:,1),x(:,2),grad_x(:,1),grad_x(:,2),'Color','k','LineWidth',3);

end