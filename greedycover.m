% Test script for gc.m
% 
% Author: Bryan Nousain, University of Maryland
% Date: December 16, 2021

close all;clear all;clc;

rng('default');
%% %%%%%%%% Define problem parameters %%%%%%%%
% Scenario parameters
number_uavs = 5;
r = 2;
gamma = 0;

% Define grid parameters
xmin = -10;
xmax = 10;
ymin = -10;
ymax = 10;
grid_incr = 5e-1;%.5e-2;

% Execution parameters
use_parallel_computing_toolbox = false;

% setting enableIterationPlots to true will show the movment of the 
% circles after every iteration
enableIterationPlots = true; 

% setting enableIterationPlots to true will show the potential circle
% movements that are being evaluated during every iteration. It will also 
% show the value of the objective function at every possible circle
% location
% Enabling subiteration plots will slow down the execution and is only 
% recommended to be enabled for debugging purposes
enableSubIterationPlots = false; 

numberWorkers = 0;

% Define solution vector:
% Points 1 to n/2 are the x coordinates, n/2+1 to n are the y coordinates
% n is the number of uavs
x0 = (xmax - xmin)*rand(2*number_uavs,1) + xmin;

%% %%%%%%%% Square region of interest %%%%%%%%
% % left/right boundary: 0 <= x <= 10
% % A*x >= b => - A*x <= -b
% leftBnd = 0;
% rightBnd = 5;
%
% % top/bottom boundary: 0 <= y <= 10
% bottomBnd = 0;
% topBnd = 5;
%
% optimInput.roi_a = [...
%     -1 0;...
%     0 -1;...
%     1 0;...
%     0 1;...
%     ]';
% optimInput.roi_b = [...
%     -leftBnd;...
%     -bottomBnd;...
%     rightBnd;...
%     topBnd];

%% %%%%%%%% Polytope region of interest %%%%%%%%
ac = [...
    2 10;...
    3 4;...
    -1 -1;...
    1 -1;...
    ]';
bc = [...
    5;...
    5;...
    5;...
    5;...
    ];

%% Run optimization
optStart = tic;
[coverPct, xd, objectiveValue, solution, exitflag, output] = gc(number_uavs,x0,r,ac,bc,gamma,xmin,xmax,ymin,ymax,grid_incr,enableIterationPlots,enableSubIterationPlots,use_parallel_computing_toolbox,numberWorkers);
execTimeSecs = toc(optStart);

% visualizeSoln(solution,optimInput.r,optimInput.xi,optimInput.xmin,optimInput.xmax,optimInput.ymin,optimInput.ymax,optimInput.grid_incr,optimInput.roi_a,optimInput.roi_b, false)