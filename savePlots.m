function savePlots(fig,plotFN)
% function savePlots(fig,plotFN)
% 
% Inputs:
%   plotFN: filename for plot, including path
%   fid: figure handle
% 
% Author: Bryan Nousain, University of Maryland
% Date: December 16, 2021

    saveas(fig,plotFN,'fig');
    saveas(fig,plotFN,'epsc');
    saveas(fig,plotFN,'png');
end