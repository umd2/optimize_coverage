# Optimize Coverage

```
cd existing_repo
git remote add origin https://gitlab.com/umd2/optimize_coverage.git
git branch -M main
git push -uf origin main
```

This repository implements 2 different numerical approaches in MATLAB to optimize the coverage area of a fleet of UAVs, while minimizing the total distance the UAVs have to travel to provide that coverage. This software was developed for a course project in ENEE 622 Convex Optimization at the University of Maryland - College Park.

The goal of this project is to characterize the trade off between attempting maximize the coverage of a region of interest while also minimizing the total distance travelled.

When plots are enabled, one can watch the decisions the optimizer is making in real time.

A detailed report describing the algorithms is [here](./docs/project_report.pdf)
## Concept
Before tasking | Response to tasking
:------: | :------:
<img src="./img/scenario_before.png" width="100%" height="100%">   | <img src="./img/scenario_after.png" width="100%" height="100%"> 

## Results
# Coverage/travel distance tradeoff

 γ = 0, Good Coverage| γ = 0.67, Shorter Travel Distance
:------: | :------:
<img src="./img/uavStartFinalPositions_xIdx-1_d5d8425.png" width="100%" height="100%"> | <img src="./img/uavStartFinalPositions_xIdx-2_d5d8425.png" width="100%" height="100%">

# Numerical analysis
 ~| ~
:------: | :------:
<img src="./img/fval_vs_gamma_fmincon_greedy.png" width="100%" height="100%"> | <img src="./img/area_vs_gamma_fmincon_greedy.png" width="100%" height="100%">
<img src="./img/percLessTotDist_vs_gamma_fmincon_greedy.png" width="100%" height="100%"> |<img src="./img/itrsConv_fmincon_greedy.png" width="100%" height="100%">

## Requirements
Both algorithm implementations require [MATLAB](https://www.mathworks.com/). The algorithms have been tested with MATLAB 2019b and 2021b.\
To use the `fmincon` version of the implemention requires the MATLAB [Optimization Toolbox](https://www.mathworks.com/products/optimization.html). It is also recommended that you have the MATLAB [Parallel Computing toolbox](https://www.mathworks.com/products/parallel-computing.html).
## Usage
There are two high levels scripts to execute each of the optimization techniques with prespecified parameters.\
 - `greedycover.m`: implements a greedy approach to minimize the objective function
 - `optimize_uav_coverage.m`: utilizes the MATLAB `fmincon` function to minimize the objective function.

## Authors and acknowledgment
Bryan Nousain, University of Maryland\
Clinton Enwerem, University of Maryland\
Mohamed Elnoor, University of Maryland
