function bulk_gamma_run(optType, gammaList, numTrials, savePath, gitShortHash)
% function bulk_gamma_run(optType, gammaList, numTrials, savePath, gitShortHash)
% 
% Inputs:
%   optType (string):   Describes which type of optimization to run.
%                       Choices are 'fmincon' or 'greedy' 
%   gammaList (double vector, dim: 1 x m): Evaluate the optimization function at 'm' values of gamma
%   numTrials (double): number of different random starting locations to use for optization algorithm
%   savePath (string): location to save results
%   gitShortHash (string):  The short hash returned by git by running the
%                           command "git rev-parse --short HEAD". This hash will be appended to the
%                           results file name for tracking the version of code that generated a
%                           specific set of results.
% 
% Outputs:
%   None
% 
% Author: Bryan Nousain, University of Maryland
% Date: December 16, 2021
rng('default')  % For reproducibility

[~,hn]=system('hostname');
fprintf('Script is currently running on: %s\n',strtrim(hn));

if ~exist(savePath,'dir')
    mkdir(savePath)
end

numGammas = length(gammaList);
% numTrials = 10;

% Scenario parameters
number_uavs = 5;
r = 2;
%% %%%%%%%% Square region of interest %%%%%%%%
% % left/right boundary: 0 <= x <= 10
% % A*x >= b => - A*x <= -b
% leftBnd = 0;
% rightBnd = 10;
% 
% % top/bottom boundary: 0 <= y <= 10
% bottomBnd = 0;
% topBnd = 10;
% 
% ac = [...
%     -1 0;... 
%     0 -1;...
%     1 0;...
%     0 1;...
%     ]';
% bc = [...
%     -leftBnd;...
%     -bottomBnd;...
%     rightBnd;...
%     topBnd];

%% %%%%%%%% Polytope region of interest %%%%%%%%
ac = [...
    2 10;...
    3 4;...
    -1 -1;...
    1 -1;...
    ]';
bc = [...
    5;...
    5;...
    5;...
    5;...
    ];

%% %%%%%%%% Grid parameters %%%%%%%%
xmin = -10;
xmax = 10;
ymin = -10;
ymax = 10;

if strcmpi(optType,'fmincon')
    grid_incr = .5e-2; % recommended size for fmincon optimization
    use_parallel_computing_toolbox = true;
elseif strcmpi(optType,'greedy')    
    grid_incr = 5e-1; % recommended size for greedy optimization
    use_parallel_computing_toolbox = false;
else
    error('The requested optimization type, %s, is not available.',optType);
end

%% %%%%%%%% Execution parameters %%%%%%%%
enablePlots = false;
numberWorkers = 12;

%% Run all
for xIdx = 1:numTrials
    x0 = (xmax - xmin)*rand(2*number_uavs,1) + xmin;

    for gIdx = 1:numGammas
        gamma = gammaList(gIdx);
        
        fn = sprintf('%s_gamma_%s_%s_indv',optType,gitShortHash,datestr(now,'yyyymmdd_HHMMSSFFF'));
        saveFN = fullfile(savePath,fn);
        
        if strcmpi(optType,'fmincon')
            optStart = tic;
            [coverPct, xd, objectiveValue, solution, exitflag, output] = ouc(number_uavs,x0,r,ac,bc,gamma,xmin,xmax,ymin,ymax,grid_incr,enablePlots,use_parallel_computing_toolbox,numberWorkers);
            execTimeSecs = toc(optStart);
        elseif strcmpi(optType,'greedy')
            optStart = tic;
            [coverPct, xd, objectiveValue, solution, exitflag, output] = gc(number_uavs,x0,r,ac,bc,gamma,xmin,xmax,ymin,ymax,grid_incr,enablePlots,use_parallel_computing_toolbox,numberWorkers);
            execTimeSecs = toc(optStart);
        else
            error('The requested optimization type, %s, is not available.',optType);
        end
        
        save(saveFN,'x0','gamma','coverPct','objectiveValue','solution','exitflag', 'output','xIdx','xd','execTimeSecs','r','xmin','xmax','ymin','ymax','grid_incr','ac','bc');
    end
end
