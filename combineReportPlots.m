% Combine plots from multiple optimization techniques
% 
% Author: Bryan Nousain, University of Maryland
% Date: December 16, 2021

close all;clc;clear all;
plotPath = './img';

fm = load('plot_vars_fmincon.mat');
gm = load('plot_vars_greedy.mat');
%% Plot objective value vs gamma from fmincon and greedy on same plot
figure;
errorbar(fm.gamma_uniq(fm.gammaIdxToPlot),fm.medObj(fm.gammaIdxToPlot),fm.stdObj(fm.gammaIdxToPlot)/2,fm.stdObj(fm.gammaIdxToPlot)/2,'LineWidth',fm.lw);
hold all;
errorbar(gm.gamma_uniq(gm.gammaIdxToPlot),gm.medObj(gm.gammaIdxToPlot),gm.stdObj(gm.gammaIdxToPlot)/2,gm.stdObj(gm.gammaIdxToPlot)/2,'LineWidth',gm.lw);

% % plot greedy lower bound guarantee
% plot(fm.gamma_uniq(fm.gammaIdxToPlot),fm.medObj(fm.gammaIdxToPlot)*(1-1/exp(1)),'LineWidth',fm.lw)

title('objective value vs \gamma - \pm 0.5 standard dev. error bars');
grid on;
set(gcf,'Color','w');
xlabel('\gamma')
ylabel('objective value');
% axis tight
% cyl = get(gca,'YLim');
% ylim([cyl(1), 101]); % limit y axis to 101

% legend({'BFGS','Greedy','Greedy lower bound'},'Location','best');
legend({'BFGS','Greedy'},'Location','best');

plotFN = fullfile(plotPath,sprintf('fval_vs_gamma_%s_%s',fm.optType,gm.optType));
savePlots(gcf,plotFN);
% NOTE: The value of the objective function for the greedy algorithm should
% be worse (higher) than the value of the objective function for the BFGS
% algorithm. However, this is not the case for some reason.
%% Plot % area covered vs gamma from fmincon and greedy on same plot
figure;
errorbar(fm.gamma_uniq(fm.gammaIdxToPlot),fm.medCvrPerc(fm.gammaIdxToPlot),fm.stdCvrPerc(fm.gammaIdxToPlot)/2,fm.stdCvrPerc(fm.gammaIdxToPlot)/2,'LineWidth',fm.lw);
hold all;
errorbar(gm.gamma_uniq(gm.gammaIdxToPlot),gm.medCvrPerc(gm.gammaIdxToPlot),gm.stdCvrPerc(gm.gammaIdxToPlot)/2,gm.stdCvrPerc(gm.gammaIdxToPlot)/2,'LineWidth',gm.lw);

% plot greedy lower bound guarantee
% plot(fm.gamma_uniq(fm.gammaIdxToPlot),fm.medCvrPerc(fm.gammaIdxToPlot)*(1-1/exp(1)),'LineWidth',fm.lw)

title('% area covered vs \gamma - \pm 0.5 standard dev. error bars');
grid on;
set(gcf,'Color','w');
xlabel('\gamma')
ylabel('% of area covered');
% axis tight
cyl = get(gca,'YLim');
ylim([cyl(1), 101]); % limit y axis to 101
legend({'BFGS','Greedy'},'Location','best');

plotFN = fullfile(plotPath,sprintf('area_vs_gamma_%s_%s',fm.optType,gm.optType));
savePlots(gcf,plotFN);
%% Plot % distance travelled vs gamma from fmincon and greedy on same plot
figure;
errorbar(fm.gamma_uniq(fm.gammaIdxToPlot),fm.medPercLessTrav(fm.gammaIdxToPlot),fm.stdPercLessTrav(fm.gammaIdxToPlot)/2,fm.stdPercLessTrav(fm.gammaIdxToPlot)/2,'LineWidth',fm.lw);
hold all;
errorbar(gm.gamma_uniq(gm.gammaIdxToPlot),gm.medPercLessTrav(gm.gammaIdxToPlot),gm.stdPercLessTrav(gm.gammaIdxToPlot)/2,gm.stdPercLessTrav(gm.gammaIdxToPlot)/2,'LineWidth',gm.lw);

grid on;
set(gcf,'Color','w');
xlabel('\gamma')
ylabel('% total distance retained');
title('% total distance retained vs \gamma - \pm 0.5 standard dev. error bars');

plotFN = fullfile(plotPath,sprintf('percLessTotDist_vs_gamma_%s_%s',fm.optType,gm.optType));
legend({'BFGS','Greedy'},'Location','best');

savePlots(gcf,plotFN);
%% Plot % area covered vs % more distance travelled
figure;
plot(fm.medPercTotTravQSrt,fm.coverPctQUniq(fm.medPercTotTravQIdx),'Marker','.','MarkerSize',fm.ms,'LineWidth',fm.lw);
hold all;
plot(gm.medPercTotTravQSrt,gm.coverPctQUniq(gm.medPercTotTravQIdx),'Marker','.','MarkerSize',gm.ms,'LineWidth',gm.lw);

title('% area covered vs % distance travelled');
grid on;
set(gcf,'Color','w');
xlabel('% distance travelled')
ylabel('% of area covered');
legend('BFGS','Greedy','Location','best');

plotFN = fullfile(plotPath,sprintf('area_vs_dist_%s_%s',fm.optType,gm.optType));
savePlots(gcf,plotFN);

%% Compare number of iterations for fmincon and greed
figure;
plot(fm.itrBinCenters,fm.itrCounts/sum(fm.itrCounts),'Marker','.','MarkerSize',fm.ms,'LineWidth',fm.lw);
hold all;
plot(gm.itrBinCenters,gm.itrCounts/sum(gm.itrCounts),'Marker','.','MarkerSize',gm.ms,'LineWidth',gm.lw);

% plot(itrBinCenters,itrCounts/sum(itrCounts),'Marker','.','MarkerSize',ms,'LineWidth',lw);

title('# of iterations until convergence');
grid on;
set(gcf,'Color','w');
xlabel('# of iterations')
ylabel('% of trials');
legend('BFGS','Greedy','Location','best');
cxl = get(gca,'XLim');
xlim([cxl(1), 250]); % limit y axis to 101

plotFN = fullfile(plotPath,sprintf('itrsConv_%s_%s',fm.optType,gm.optType));
savePlots(gcf,plotFN);
