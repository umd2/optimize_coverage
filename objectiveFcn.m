function f = objectiveFcn(x,optimInput)
% function f = objectiveFcn(x,optimInput)
%
% Inputs:
%   x (double vector, dim: 2*n x 1): matrix of circle centers [x1;x2;...;xn;y1;y2;...;yn], indicies 1 to n/2 are the x coordinates, n/2+1 to n are the y coordinates: 
%   optimInput.r (double): radius of circles (scalar)
%   optimInput.xmin (double): minimum x value in grid
%   optimInput.xmax (double): maximum x value in grid
%   optimInput.ymin (double): minimum y value in grid
%   optimInput.ymax (double): maximum y value in grid
%   optimInput.grid_incr (double): separation between grid points for x and y
%   optimInput.roi_a (double matrix, dim: m x 2): matrix 'a' where constraint 'i' is of the form a(1,i)*x + a(2,i)*y <= b(i)
%   optimInput.roi_b (double matrix, dim: m x 2): vector 'b' where constraint 'i' is of the form a(1,i)*x + a(2,i)*y <= b(i)
%   optimInput.xi (double vector, dim: 2*n x 1): starting positions of each circle, indicies 1 to n/2 are the x coordinates, n/2+1 to n are the y coordinates
%   optimInput.gamma (double vector, dim: 2*n x 1): weighting to place on sum of distance travelled
% Outputs:
%   f (double): value of objective function
% 
% Author: Bryan Nousain, University of Maryland
% Date: December 16, 2021
%% Input arguments:
%
% Example input:
%   x = [0 0 2 2 10 1]';
%   optimInput.xmin = -7;
%   optimInput.xmax = 5;
%   optimInput.ymin = -6;
%   optimInput.ymax = 5;
%   optimInput.grid_incr = 1e-2;
%   optimInput.r = 2;
%   optimInput.roi_a = [2 10;3 4;-1 -1;1 -1]';
%   optimInput.roi_b = [5;5;5;5];
%   optimInput.xi = [-5 9 3 -6 8 -4]';
%   optimInput.gamma = 1e-1;

%% %%%%%%%% Create mesh to evaluate conditions on all points %%%%%%%%

[grid_x, grid_y] = meshgrid(optimInput.xmin:optimInput.grid_incr:optimInput.xmax,...
    optimInput.ymin:optimInput.grid_incr:optimInput.ymax);
% Note: for large grids this calculation could be sped up significantly by
% automatically shrinking the grid to the largest rectangle that encloses
% the region of interest

%% %%%%%%%% Calculate if each grid point meets constraints %%%%%%%%

% Put coordinates of each circle in 3rd dimension to speed up processing
aax = reshape(optimInput.roi_a(1, :), 1, 1, size(optimInput.roi_a, 2));
aay = reshape(optimInput.roi_a(2, :), 1, 1, size(optimInput.roi_a, 2));
bb = reshape(optimInput.roi_b, 1, 1, length(optimInput.roi_b));

% Check whether all constraints are met
constrnt = aax.*grid_x + aay.*grid_y <= bb;
constrntMet = all(constrnt,3);

%% %%%%%%%% Visualize constraints %%%%%%%%
% imagesc(grid_x(1,:),grid_y(:,1),constrntMet)
% set(gca,'YDir','normal');
%
% % Plot constraints
% const_color = 'g';
% const_lw = 3;
% hold all;
% ac = optimInput.roi_a;bc=optimInput.roi_b;
% for ai = 1:size(ac,2)
%     lgnd{ai} = sprintf('%d *x + %d*y + %d\n',ac(1,ai),ac(2,ai),-bc(ai));
%
%     if ac(2,ai) == 0 % this means the boundary is a verical line
%         lh = line([bc(ai)/ac(1,ai) bc(ai)/ac(1,ai)],[min(grid_y(1:end,1)) max(grid_y(1:end,1))],'Color',const_color,'LineWidth',const_lw);
%     else
%         plot(grid_x(1,1:end),-ac(1,ai)/ac(2,ai)*grid_x(1,1:end) + bc(ai)/ac(2,ai),const_color,'LineWidth',const_lw);hold all;
%     end
% end

%% %%%%%%%% Calculate if each grid point is in union of circles %%%%%%%%

% Reshape x from vector to matrix
x = [x(1:end/2), x(end/2+1:end)];

% Put coordinates of each circle in 3rd dimension to speed up processing
xx = reshape(x(:, 1), 1, 1, size(x, 1));
yy = reshape(x(:, 2), 1, 1, size(x, 1));

% Check whether each point is in union of circles
dist = (grid_x - xx).^2 + (grid_y - yy).^2;
pointInCircle = dist <= optimInput.r^2;
in_union = any(pointInCircle,3);

% Find the intersection between the union of circles and the ROI
circleUnionIntersectROI = constrntMet & in_union;
%% %%%%%%%% Calculate sum of (travelled distance)^2  %%%%%%%%
xi = [optimInput.xi(1:end/2), optimInput.xi(end/2+1:end)];

% when calculating actual distance (e.g. sqrt) the approximated gradients
% are too small and the optimization stops prematurely
% xd = sum(sqrt(sum((x - xi).^2,2)));

xd2 = sum(sum((x - xi).^2,2));

%% %%%%%%%% Calculate objective %%%%%%%%
% Note that the actual area of the ROI is optimInput.grid_incr^2*constrntArea
constrntArea = sum(sum(constrntMet));

f = -sum(sum(circleUnionIntersectROI))/constrntArea*1e2 + optimInput.gamma*xd2;
end