% Generate optimization parameters to use in Python optimization script.
% 
% Author: Bryan Nousain, University of Maryland
% Date: December 16, 2021

clear all;clc;
rng('default')  % For reproducibility
%% Basic configuration
n = 5; % number of uavs
r = 2; % sensing radius
%% %%%%%%%% Square region of interest %%%%%%%%
% the ith constraint is: a(1,i)*x + a(2,i)*y <= b(i)
% left/right boundary: 0 <= x <= 10
leftBnd = 0;
rightBnd = 10;

% top/bottom boundary: 0 <= y <= 10
bottomBnd = 0;
topBnd = 10;

squareROI.a = [...
    -1 0;...
    0 -1;...
    1 0;...
    0 1;...
    ]';
squareROI.b = [...
    -leftBnd;...
    -bottomBnd;...
    rightBnd;...
    topBnd];

%% %%%%%%%% Polytope region of interest %%%%%%%%
% the ith constraint is: a(1,i)*x + a(2,i)*y <= b(i)
polytopeROI.a = [...
    2 10;...
    3 4;...
    -1 -1;...
    1 -1;...
    ]';
polytopeROI.b = [...
    5;...
    5;...
    5;...
    5;...
    ];


%% %%%%%%%% Initial starting positions %%%%%%%%
xmin = -10;
xmax = 10;
ymin = -10;
ymax = 10;
grid_incr = 1e-2;
numTrials = 50;

x0 = zeros(2*n,numTrials);
for trialIdx = 1:numTrials
    x0(:,trialIdx) = (xmax - xmin)*rand(2*n,1) + xmin;
end

% how to convert x0 from vector to matrix format
% indicies 1 to n/2 are the x coordinates, n/2+1 to n are the y coordinates
% x0 = [x0(1:end/2,i), x0(end/2+1:end,i)]; 

%% Generate gamma
min_gamma = 0;
max_gamma = 2;

gammaList = linspace(min_gamma,max_gamma,150);%min_gamma:incr_gamma:max_gamma;

% Round to 4 significant digits
gammaList = floor(gammaList*1e4)*1e-4;
%% Save all variables to remain compatible with Python SciPy
% https://docs.scipy.org/doc/scipy/reference/generated/scipy.io.loadmat.html

save('opt_params','n','x0','gammaList','squareROI','polytopeROI','-v7'); 


