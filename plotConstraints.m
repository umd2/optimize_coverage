function plotConstraints(ac,bc,xmin,xmax,ymin,ymax,grid_incr,const_color,const_lw)

[grid_x, grid_y] = meshgrid(xmin:grid_incr:xmax, ymin:grid_incr:ymax);

if nargin < 8
    const_color = 'k';
end
if nargin < 9
    const_lw = 3;
end
for ai = 1:size(ac,2)
    %     lgnd{ai} = sprintf('%d *x + %d*y + %d\n',ac(1,ai),ac(2,ai),-bc(ai));
    
    if ac(2,ai) == 0 % this means the boundary is a verical line
        line([bc(ai)/ac(1,ai) bc(ai)/ac(1,ai)],[min(grid_y(1:end,1)) max(grid_y(1:end,1))],'Color',const_color,'LineWidth',const_lw);
    else
        plot(grid_x(1,1:end),-ac(1,ai)/ac(2,ai)*grid_x(1,1:end) + bc(ai)/ac(2,ai),const_color,'LineWidth',const_lw);hold all;
    end
end