function visualizeSoln(solution,r,xi,xmin,xmax,ymin,ymax,grid_incr,ac,bc, newFigure)
% function visualizeSoln(solution,r,xi,xmin,xmax,ymin,ymax,grid_incr,ac,bc)
% 
% Inputs:
% solution (double vector, dim: 2*n x 1): final positions of each UAV, indicies 1 to n/2 are the x coordinates, n/2+1 to n are the y coordinates
% r (double): sensing radius of individual UAV
% xi (double vector, dim: 2*n x 1): starting positions of each UAV, indicies 1 to n/2 are the x coordinates, n/2+1 to n are the y coordinates
% xmin (double): minimum x coordinate to consider during optimization
% xmax (double): maximum x coordinate to consider during optimization
% ymin (double): minimum y coordinate to consider during optimization
% ymax (double): maximum y coordinate to consider during optimization
% grid_incr (double): increment of grid  between xmin and xmax and ymin and ymax
% ac (double matrix, dim: m x 2): Specifies the region of interest. For m constraints, the ith constrain ac(1,i)*x + ac(2,i)*y <= bc(i)
% bc (double matrix, dim: m x 2): Specifies the region of interest. For m constraints, the ith constrain ac(1,i)*x + ac(2,i)*y <= bc(i)
% newFigure (bool, default true): If true, a new figure window will be opened

if nargin < 11
    newFigure = true;
end

number_uavs = length(solution)/2;

% Create mesh to evaluate conditions on all points
[grid_x, grid_y] = meshgrid(xmin:grid_incr:xmax, ymin:grid_incr:ymax);

%% %%%%%%%% Calculate if each grid point is in the region of interest (ROI) %%%%%%%%
aax = reshape(ac(1, :), 1, 1, size(ac, 2));
aay = reshape(ac(2, :), 1, 1, size(ac, 2));
bb = reshape(bc, 1, 1, length(bc));

constrnt = aax.*grid_x + aay.*grid_y <= bb;
constrntMet = all(constrnt,3);

%% %%%%%%%% Calculate if each grid point is in union of circles %%%%%%%%
% Reshape x from vector to matrix
x = [solution(1:end/2), solution(end/2+1:end)];

% Put coordinates of each circle in 3rd dimension to speed up processing
xx = reshape(x(:, 1), 1, 1, size(x, 1));
yy = reshape(x(:, 2), 1, 1, size(x, 1));

% Check whether each point is in union of circles
dist = (grid_x - xx).^2 + (grid_y - yy).^2;
pointInCircle = dist <= r^2;
in_union = any(pointInCircle,3);

%% %%%%%%%% Find the intersection between the union of circles and the region of interest (ROI) %%%%%%%%
% circleUnionIntersectROI = constrntMet & in_union;

% maxScale = 255;
allC = constrntMet + 2*in_union;% + circleUnionIntersectROI;
% allC = (maxScale/2)*constrntMet + (maxScale/2)*in_union;

if newFigure
    figure;
end

imagesc(grid_x(1,:),grid_y(:,1),allC)
set(gca,'YDir','normal');

%% Create custom color map
% Union of circles not intersecting with region of interest: yellow
% Union of circles intersecting with region of interest: green
% Region of interest: light blue, black if no circles intersect ROI
cmap = zeros(255,3); % make everything black
cmap(1,:) = ones(1,3); % make 0 white
cmap(255/3+1,:) = [0.1778 0.5349  0.9641];% make 1 light blue, cc(75,:);
cmap(2/3*255+1,:) = [0.9608 0.8902 0.1532];% make 2 yellow, cc(240,:);
cmap(end,:) = [0.1373 0.6941 0.2980]; % make 3 green
colormap(cmap);
%% %%%%%%%% Plot constraint functions defining region of interest (ROI) %%%%%%%%
const_color = 'k';
const_lw = 3;
hold all;
plotConstraints(ac,bc,xmin,xmax,ymin,ymax,grid_incr,const_color,const_lw)

% for ai = 1:size(ac,2)
%     %     lgnd{ai} = sprintf('%d *x + %d*y + %d\n',ac(1,ai),ac(2,ai),-bc(ai));
%     
%     if ac(2,ai) == 0 % this means the boundary is a verical line
%         line([bc(ai)/ac(1,ai) bc(ai)/ac(1,ai)],[min(grid_y(1:end,1)) max(grid_y(1:end,1))],'Color',const_color,'LineWidth',const_lw);
%     else
%         plot(grid_x(1,1:end),-ac(1,ai)/ac(2,ai)*grid_x(1,1:end) + bc(ai)/ac(2,ai),const_color,'LineWidth',const_lw);hold all;
%     end
% end
%% %%%%%%%% Visualize solution %%%%%%%%
% Plot starting locations
xi = [xi(1:end/2), xi(end/2+1:end)]; % convert from vector to matrix format
plot(xi(:, 1), xi(:, 2), 'k*');hold all;
viscircles(xi, r .* ones(number_uavs, 1),'Color',.5*ones(1,3),'LineStyle','--');

% Plot finish locations
solution = [solution(1:end/2), solution(end/2+1:end)]; % convert from vector to matrix format
plot(solution(:, 1), solution(:, 2), 'r*');
viscircles(solution, r .* ones(number_uavs, 1));

% Plot arrows from starting location to finish location
xd = solution - xi;
quiver(xi(:,1),xi(:,2),xd(:,1),xd(:,2),0,'Color','k','LineWidth',3);
set(gcf,'Color','w');
axis square
end
