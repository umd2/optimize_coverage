function [n_idx,xyCoords,fMin] = greedyNextMove(n,x,optimInput,enablePlots,iterNum)
% function [n_idx,xyCoords,fMin] = greedyNextMove(n,x,optimInput)
%
% Inputs:
%   n (double vector, dim: 2*n x 1): number of circles 
%   x (double vector, dim: 2*n x 1): matrix of circle centers [x1;x2;...;xn;y1;y2;...;yn], indicies 1 to n/2 are the x coordinates, n/2+1 to n are the y coordinates
%   optimInput.r (double): radius of circles (scalar)
%   optimInput.xmin (double): minimum x value in grid
%   optimInput.xmax (double): maximum x value in grid
%   optimInput.ymin (double): minimum y value in grid
%   optimInput.ymax (double): maximum y value in grid
%   optimInput.grid_incr (double): separation between grid points for x and y
%   optimInput.roi_a (double matrix, dim: m x 2): matrix 'a' where constraint 'i' is of the form a(1,i)*x + a(2,i)*y <= b(i)
%   optimInput.roi_b (double matrix, dim: m x 2): vector 'b' where constraint 'i' is of the form a(1,i)*x + a(2,i)*y <= b(i)
%   optimInput.xi (double vector, dim: 2*n x 1): starting positions of each circle, indicies 1 to n/2 are the x coordinates, n/2+1 to n are the y coordinates
%   optimInput.gamma (double vector, dim: 2*n x 1): weighting to place on sum of distance travelled
%   enablePlots (boolean):  If true will display plots showing potential circle locations and the objective function value for every possible
%                           location of each circle
%   iterNum (double), OPTIONAL: The iteration number of the optimization routing. If provided, it will display the iteration number of the plots
% 
% Outputs:
%   n_idx (double): the index of the circle that moved
%   xyCoords (double 1 x 2): the new x and y coordinates for the cirlce
%   fMin (double): value of objective function
% 
% Author: Bryan Nousain, University of Maryland
% Date: December 16, 2021

if nargin < 5
    iterNum = [];
end
%% Create search grid
x_range = optimInput.xmin:optimInput.grid_incr:optimInput.xmax;
y_range = optimInput.ymin:optimInput.grid_incr:optimInput.ymax;

fGrid = zeros(length(x_range),length(y_range));
fMinCircs = zeros(n,1);
fMinNewCoords = zeros(n,2);
for circIdx = 1:n
    x0scan = x;

    % search through all points in grid and determine which new location
    % will minimize the objective function
    for gxIdx = 1:length(x_range)
        for gyIdx = 1:length(y_range)
            x0scan(circIdx) = x_range(gxIdx); % set x coordinate
            x0scan(n+circIdx) = y_range(gyIdx);% set y coordinate
            
            fGrid(gyIdx,gxIdx) = objectiveFcn(x0scan,optimInput);
        end
    end

    if enablePlots
        figure(2);
        imagesc(x_range,y_range,fGrid);set(gca,'YDir','normal');hold all;
        plotConstraints(optimInput.roi_a,optimInput.roi_b,optimInput.xmin,optimInput.xmax,optimInput.ymin,optimInput.grid_incr:optimInput.ymax,optimInput.grid_incr);
        colorbar;
        if isempty(iterNum)
            title(sprintf('Objective values for circle %d',circIdx));
        else
            title(sprintf('Iteration %d - Objective values for circle %d - ',iterNum,circIdx));
        end
    end
    
    [fMinX, fMinXYIdx] = min(fGrid,[],2);
    [fMinY, fMinYIdx] = min(fMinX);
    fMinCircs(circIdx) = fMinY;

    fMinXIdx = fMinXYIdx(fMinYIdx);
    fMinNewCoords(circIdx,:) = [x_range(fMinXIdx), y_range(fMinYIdx)];
    
    if enablePlots
        solutionIter = x;
        solutionIter(circIdx) = fMinNewCoords(circIdx,1);
        solutionIter(n + circIdx) = fMinNewCoords(circIdx,2);
        
        figure(3);
        visualizeSoln(solutionIter,optimInput.r,optimInput.xi,optimInput.xmin,optimInput.xmax,optimInput.ymin,optimInput.ymax,optimInput.grid_incr,optimInput.roi_a,optimInput.roi_b, false)
        
        if isempty(iterNum)
            title(sprintf('Evaluate moving circle %d',circIdx));
        else
            title(sprintf('Iteration %d - Evaluate moving circle %d',iterNum,circIdx));
        end
    end
end % Recommend stopping debugger at this line to see the visualization of the possible moves being evaluated
% Choose to move the circle that has the minimal value of the objective
fMin = min(fMinCircs);

% look for ties
allMinIdx = find(fMinCircs == fMin);

% randomly choose one of the indicies that tied for the lowest
randIdx = ceil(length(allMinIdx)*rand(1));

n_idx = allMinIdx(randIdx);

xyCoords = fMinNewCoords(n_idx,:);