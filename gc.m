function [coverPct, xd, objectiveValue, solution, exitflag, output] = gc(n,x0,r,ac,bc,gamma,xmin,xmax,ymin,ymax,grid_incr,enableIterationPlots,enableSubIterationPlots,use_parallel_computing_toolbox,numberWorkers)
% function [coverPct, xd, objectiveValue, solution, exitflag, output] = gc(n,x0,r,ac,bc,gamma,xmin,xmax,ymin,ymax,grid_incr,enablePlots,use_parallel_computing_toolbox,numberWorkers)
% 
% Inputs:
%   n (double): number of unmanned air vehicles (UAVs) to provide coverage
%   x0 (double vector, dim: 2*n x 1): starting positions of each UAV, indicies 1 to n/2 are the x coordinates, n/2+1 to n are the y coordinates
%   r (double): sensing radius of individual UAV
%   ac (double matrix, dim: m x 2): Specifies the region of interest. For m constraints, the ith constrain ac(1,i)*x + ac(2,i)*y <= bc(i)
%   bc (double matrix, dim: m x 2): Specifies the region of interest. For m constraints, the ith constrain ac(1,i)*x + ac(2,i)*y <= bc(i)
%   gamma (double): Weight placed on total distance travelled
%   xmin (double): minimum x coordinate to consider during optimization
%   xmax (double): maximum x coordinate to consider during optimization
%   ymin (double): minimum y coordinate to consider during optimization
%   ymax (double): maximum y coordinate to consider during optimization
%   grid_incr (double): increment of grid  between xmin and xmax and ymin and ymax
%   enableIterationPlots (boolean): if true will plot will show the movment of the circles after every iteration
%   enableSubIterationPlots (boolean):  if true will show the potential circle movements 
%                                       that are being evaluated during every iteration and
%                                       the value of the objective function at every possible circle. This will
%                                       slow down program execution, especially for large numbers of UAVs
%   use_parallel_computing_toolbox (boolean): if true will use parallel computing toolbox
%   numberWorkers (double) [OPTIONAL]: number of workers for parallel computing toolbox to use. The default number of workers is 4. (NOT YET IMPLEMENTED)
% 
% Outputs:
%   coverPct (double): percentage of region of interest covered
%   xd (double): total distance travelled
%   objectiveValue (double): objective value at end of optimization
%	exitflag (double)   0 Optimization prematurely stopped because maximum number of iterations was reached. Optimal solution was possibly not found.
%                       2 Reached threshold for consecutive number of iterations below stopping tolerance. Optimal solution found.
%%
if nargin < 14
    if use_parallel_computing_toolbox
        numberWorkers = 4;
    else
        numberWorkers = 0;
    end
end
if use_parallel_computing_toolbox
    % Shutting down the pool in case one is accidentially left open
    p = gcp('nocreate');
    
    if ~isempty(p)
        delete(gcp)
    end
end
optimInput.r = r;
optimInput.xmin = xmin;
optimInput.xmax = xmax;
optimInput.ymin = ymin;
optimInput.ymax = ymax;
optimInput.grid_incr = grid_incr;
optimInput.roi_a = ac;
optimInput.roi_b = bc;
optimInput.xi = x0;
optimInput.gamma = gamma;

%% %%%%%%%% Define problem parameters %%%%%%%%
% % Scenario parameters
% n = 5;
% r = 2;
% gamma = .1;
% 
% % Define grid parameters
% xmin = -10;
% xmax = 10;
% ymin = -10;
% ymax = 10;
% grid_incr = 5e-1;%.5e-2;
% 
% % Execution parameters
% % use_parallel_computing_toolbox = false;
% % enablePlots = true;
% numberWorkers = 0;

% % Define solution vector:
% % Points 1 to n/2 are the x coordinates, n/2+1 to n are the y coordinates
% % n is the number of uavs
% x0 = (xmax - xmin)*rand(2*n,1) + xmin;

%% %%%%%%%% Square region of interest %%%%%%%%
% % left/right boundary: 0 <= x <= 10
% % A*x >= b => - A*x <= -b
% leftBnd = 0;
% rightBnd = 5;
%
% % top/bottom boundary: 0 <= y <= 10
% bottomBnd = 0;
% topBnd = 5;
%
% roi_a = [...
%     -1 0;...
%     0 -1;...
%     1 0;...
%     0 1;...
%     ]';
% roi_b = [...
%     -leftBnd;...
%     -bottomBnd;...
%     rightBnd;...
%     topBnd];

%% %%%%%%%% Polytope region of interest %%%%%%%%
% roi_a = [...
%     2 10;...
%     3 4;...
%     -1 -1;...
%     1 -1;...
%     ]';
% roi_b = [...
%     5;...
%     5;...
%     5;...
%     5;...
%     ];


% [grid_x, grid_y] = meshgrid(xmin:grid_incr:xmax,...
%     ymin:grid_incr:ymax);
%% Run optimization
% For each UAV evaluate where it should move to in order give the maximal
% gain
maxNumIterations = 1e3;
stoppingTol = 1e-6;
iterStuckThreshold = 5; % maximum number of iterations to continue after fVal stops decreasing more than the tolerance

% initialize
x = x0;
fMinArch = zeros(maxNumIterations,1);
fValUnchangedIterCount = 0;

for iterNum = 1:maxNumIterations
    fprintf('Iteration %d ',iterNum);
    
    % Find the next step in a greedy fashion
    [n_idx,xyCoords,fMin] = greedyNextMove(n,x,optimInput,enableSubIterationPlots,iterNum);
    
    % Change the coordinates of the circle that yielded the biggest gains
    x(n_idx) = xyCoords(1); % set x coordinate
    x(n + n_idx) = xyCoords(2);% set y coordinate
    
    % Keep track of fMin
    fMinArch(iterNum) = fMin;
    
    fprintf('Move Circle %d to (%.2f, %.2f), fval = %.4f\n',n_idx,xyCoords(1),xyCoords(2),fMin);
    
    if enableIterationPlots
        figure(1);
        visualizeSoln(x,r,x0,xmin,xmax,ymin,ymax,grid_incr,ac,bc, false)
        title(sprintf('Iteration %d final decision: Move circle %d',iterNum,n_idx));
    end
    
    if iterNum  > 1
        if abs(fMinArch(iterNum) - fMinArch(iterNum-1)) <= stoppingTol
                    fValUnchangedIterCount = fValUnchangedIterCount + 1;
        else
            fValUnchangedIterCount = 0; % reset count
        end
        if abs(fMinArch(iterNum) - fMinArch(iterNum-1)) <= stoppingTol && fValUnchangedIterCount >= iterStuckThreshold
            fprintf('Reached threshold for consecutive number of iterations below stopping tolerance. Optimal solution found.\n');
            break;
        end
    end        
end

% create exitflag conditions that are consistent with fmincon
if iterNum == maxNumIterations
    exitflag = 0;
else
    exitflag = 2; 
end

output.iterations = iterNum;
solution = x;
objectiveValue = fMin;

solution_2C = [solution(1:end/2), solution(end/2+1:end)];
x0_2C = [x0(1:end/2), x0(end/2+1:end)];
        
xd = sum(sqrt(sum((solution_2C - x0_2C).^2,2))); % sum of distance travelled
xd2 = sum(sum((solution_2C - x0_2C).^2,2)); % sum of (distance travelled)^2
coverPct = -1*(objectiveValue - optimInput.gamma*xd2);

% visualizeSoln(solution,r,x0,xmin,xmax,ymin,ymax,grid_incr,roi_a,roi_b, false)